import { Injectable } from '@angular/core';
import { API } from './api';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class UserService {

    constructor(private _http: HttpClient) { }

    get(identification: number) {
        return this._http.get(`${API}/user/${identification}`);
    }

    store(user: any) {
        return this._http.post(`${API}/user/store`, user);
    }

}