import { Injectable } from '@angular/core';
import { API } from './api';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class RequestService {

    constructor(private _http: HttpClient) { }

    // get(identification: number) {
    //     return this._http.get(`${API}/request/${identification}`);
    // }

    store(request: any) {
        return this._http.post(`${API}/request/store`, request);
    }

}