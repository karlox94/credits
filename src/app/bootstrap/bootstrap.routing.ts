import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BootstrapComponent } from './bootstrap.component';

const routes: Routes = [
  // { path: '', component: BootstrapComponent },
  { path: '', loadChildren: '../register/register.module#RegisterModule' },
  { path: 'request', loadChildren: '../request/request.module#RequestModule' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class BootstrapRoutingModule { }
