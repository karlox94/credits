import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BootstrapRoutingModule } from './bootstrap.routing';
import { BootstrapComponent } from './bootstrap.component';
import { RouterModule } from '@angular/router';
import { MatToolbarModule, MatButtonModule } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

@NgModule({
  imports: [
    CommonModule,
    BootstrapRoutingModule,
    BrowserAnimationsModule,
    RouterModule,
    MatToolbarModule,
    MatButtonModule
  ],
  declarations: [
    BootstrapComponent
  ],
  bootstrap: [
    BootstrapComponent
  ]
})
export class BootstrapModule { }
