export class User {

    constructor(
      public identification?: string,
      public name?: string,
      public last_name?: string,
      public birth_date?: string
    ) {  }
  
  }