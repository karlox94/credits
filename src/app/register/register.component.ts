import { Component } from '@angular/core';
import { FormGroup, FormControl, Validators, NgForm, FormGroupDirective, AbstractControl } from '@angular/forms';
import { User } from './models/user.model';
import { ErrorStateMatcher } from '@angular/material';
import { DatePipe } from '@angular/common';

import { UserService } from '../shared/services/user.service';

export class MyErrorStateMatcher implements ErrorStateMatcher {
    isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
        const isSubmitted = form && form.submitted;
        return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
    }
}

const VALIDATIONS = {
    isRequired: 'Este campo es requerido',
    isdate: 'Esto no es una fecha',
    isNumber: 'Solo números',
    isBoy: 'No eres mayor de edad',
    isClient: 'Número de Identificación ya existe'
};

@Component({
    selector: 'app-register',
    templateUrl: './register.component.html',
    styleUrls: ['./register.component.css'],
    providers: [DatePipe]
})
export class RegisterComponent {

    private _validations = VALIDATIONS;
    private _user: User = new User();
    private _showForm: boolean = true;
    private _submited: boolean;
    private _isClient: boolean;

    private _registerForm: FormGroup = new FormGroup({
        identification: new FormControl('', [
            Validators.required,
            Validators.pattern('^[0-9]*$'),
            this.validateIdentification.bind(this)
        ]),
        name: new FormControl('', [
            Validators.required
        ]),
        lastName: new FormControl('', [
            Validators.required
        ]),
        birthDate: new FormControl('', [
            Validators.required,
            this.validateYearsOld
        ])
    });

    matcher = new MyErrorStateMatcher();

    constructor(private _datePipe: DatePipe,
        private _userService: UserService) { }

    onSubmit() {
        this._submited = true;
        this._user.birth_date = this._datePipe.transform(this._user.birth_date, 'yyyy-MM-dd');
        this._userService.store(this._user).subscribe((res: any) => {
            // console.log(res);
            this.resetForm();
        });
    }

    validateYearsOld(control: FormControl) {
        let today: any = new Date();
        let birthDate: any = new Date(control.value);
        let yearsOld = today - birthDate;
        let daysBetween = Math.floor(yearsOld / (1000 * 60 * 60 * 24));  

        if (daysBetween < 6570) {
            return {
                adulthood: {
                    adult: yearsOld
                }
            }
        }
        return null;
    }

    resetForm() {
        this._registerForm.reset();
        this._showForm = false;
        this._isClient = false;
        this._user.birth_date = '';
        setTimeout(() => {
            this._showForm = true;
        });
    }

    validateIdentification(control: FormControl) {
        this._userService.get(+control.value).subscribe((res: any) => {
            // console.log(res);
            if (res && res.length > 0) {
                this._isClient = true;
            } else {
                this._isClient = false;
            }
        });
    }

}