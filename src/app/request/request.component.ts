import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, NgForm, FormGroupDirective, AbstractControl } from '@angular/forms';
import { Request } from './models/request.model';
import { ErrorStateMatcher } from '@angular/material';
import { DatePipe } from '@angular/common';
import { UserService } from '../shared/services/user.service';
import { RequestService } from '../shared/services/request.service';

export class MyErrorStateMatcher implements ErrorStateMatcher {
    isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
        const isSubmitted = form && form.submitted;
        return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
    }
}

const VALIDATIONS = {
    isRequired: 'Este campo es requerido',
    isToday: 'La fecha debe ser antes que hoy',
    isNumber: 'Solo números',
    less: 'Salario debe ser menor a $100.000.000',
    higher: 'Salario debe ser mayor a $0',
    isClient: 'Número de Identificación no existe'
};

@Component({
    selector: 'app-request',
    templateUrl: './request.component.html',
    styleUrls: ['./request.component.css'],
    providers: [DatePipe]
})
export class RequestComponent {

    private _validations = VALIDATIONS;
    private _request: Request = new Request();
    private _showForm: boolean = true;
    private _isClient: boolean;
    private _submited: boolean;
    private _userIdentification: string;
    private _checkDigit: number;
    private _response: any;

    private _requestForm: FormGroup = new FormGroup({
        identification: new FormControl('', [
            Validators.required,
            Validators.pattern('^[0-9]*$'),
            this.validateIdentification.bind(this)
        ]),
        companyNIT: new FormControl('', [
            Validators.required,
            Validators.pattern('^[0-9]*$')
        ]),
        companyName: new FormControl('', [
            
        ]),
        salary: new FormControl('', [
            Validators.required,
            Validators.pattern('^[0-9]*$'),
            Validators.max(99999999),
            Validators.min(1),
        ]),
        dateHire: new FormControl('', [
            Validators.required,
            this.validateDateHire,
            // this.validateWorkingTime
        ]),
        checkDigit: new FormControl({value: '', disabled: true}, [ ])
    });

    matcher = new MyErrorStateMatcher();

    constructor(private _datePipe: DatePipe,
        private _userService: UserService,
        private _requestService: RequestService) {}

    onSubmit() {
        this._request.date_hire = this._datePipe.transform(this._request.date_hire, 'yyyy-MM-dd');
        this._requestService.store(this._request).subscribe((res: any) => {
            // console.log(res);
            this._submited = true;
            this._response = res;
            this.resetForm();
        });
    }

    validateDateHire(control: FormControl) {        
        let today: Date = new Date();
        today = new Date(today.getUTCFullYear(), today.getUTCMonth(), today.getDate(), 0, 0 ,0);
        let dateHire: Date = new Date(control.value);     
        
        if (dateHire >= today) {
            return {
                isToday: {
                    day: dateHire
                }
            }
        } 
        return null;
    }

    resetForm() {
        this._requestForm.reset();
        this._showForm = false;
        setTimeout(() => {
            this._isClient = false;
            this._showForm = true;
        });
    }

    validateIdentification(control: FormControl) {
        this._userService.get(+control.value).subscribe((res: any) => {
            if (res.length > 0 ) {
                this._request.user_id = res[0].id;
                // console.log(this._request.user_id );
            }
            if (typeof (control.value) != 'undefined' && res && res.length == 0 && control.value != null) {
                this._isClient = true;
            } else {
                this._isClient = false;
            }
        });
    }

    calculateCheckDigit() {
        if (this._request.company_NIT) {
            let number = new Array(16);
            let z = String(this._request.company_NIT).length;
            
    
            number[1] = 3;
            number[2] = 7;
            number[3] = 13;
            number[4] = 17;
            number[5] = 19;
            number[6] = 23;
            number[7] = 29;
            number[8] = 37;
            number[9] = 41;
            number[10] = 43;
            number[11] = 47;
            number[12] = 53;
            number[13] = 59;
            number[14] = 67;
            number[15] = 71;
    
            let x = 0;
            let y = 0;
            for (var i = 0; i < z; i++) {
                y = (+String(this._request.company_NIT).substr(i, 1));
                x += (y * number[z - i]); 
            }
    
            y = x % 11;

            this._checkDigit = (y > 1) ? 11 - y : y;
        }
    }

    // validateWorkingTime(control: FormControl) {
    //     let today: any = new Date();
    //     let dateHire: any = new Date(control.value);
    //     let wt = today - dateHire;
    //     let daysBetween = Math.floor(wt / (1000 * 60 * 60 * 24));        
        
    //     if (daysBetween < 547) {
    //         return {
    //             workingTime: {
    //                 date: wt
    //             }
    //         }
    //     }
    //     return null;
    // }

}