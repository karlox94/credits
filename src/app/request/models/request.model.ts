export class Request {

    constructor(
      public company_NIT?: number,
      public company_name?: string,
      public salary?: number,
      public date_hire?: string,
      public user_id?: number,
      public state?: number,
      public amount?: number
    ) {  }
  
  }