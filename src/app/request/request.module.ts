import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RequestComponent } from './request.component';
import { RequestRoutingModule } from './request.routing';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {
    MatFormFieldModule,
    MatInputModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatButtonModule
} from '@angular/material';
import { RequestService } from '../shared/services/request.service';
import { UserService } from '../shared/services/user.service';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
    declarations: [
        RequestComponent,
    ],
    imports: [
        CommonModule,
        RequestRoutingModule,
        MatFormFieldModule,
        MatInputModule,
        MatDatepickerModule,
        MatNativeDateModule,
        MatButtonModule,
        FormsModule,
        ReactiveFormsModule,
        HttpClientModule
    ],
    exports: [],
    providers: [
        RequestService,
        UserService
    ],
})
export class RequestModule { }